Monitor Daemon
============

Monitor Daemon is a Ruby system application designed to help monitor various operations on a computer system. Out of the box there is a system monitor plugin which logs system specific information at the predefined interval (set in the config.yml).

Usage
-----

To start the daemon run:
> ./monitor.rb start

To stop:
> ./monitor.rb stop