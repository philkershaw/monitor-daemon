# Monitor.rb
# 
# The entry point for the monitor daemon. Loads in plugins from the plugins/ 
# directory as specified in the config.yml. Invokes the execute method of each
# plugin class at the interval specified in the config.
#
# Author:: Phil Kershaw <mailto:p.a.kershaw@gmail.com>

require 'yaml'
require 'daemons'

class Monitor
    def initialize
        base_dir  = File.absolute_path(File.dirname(__FILE__))
        $log_path = File.join(base_dir, "monitor-logs")
        # load config file 
        config = YAML::load_file(File.join(base_dir, "config.yml"))
        
        # load plugins as per config
        plugins = Array.new
        config['Plugins'].each { |p|
            require File.join(base_dir, "plugins", p, "#{p.downcase}.rb")
            plugins << p
        }
        
        action = (ARGV[0].nil?) ? 'start' : ARGV[0]
        daemon_options = {
            :ARGV       => [action, '-f', '--'],
            :dir_mode   => :normal,
            :dir        => 'pids',
            :multiple   => false,
            :ontop      => false,
            :mode       => :load,
            :backtrace  => true,
            :monitor    => true,
            :log_dir    => $log_path
        }
        Daemons.run_proc('Monitor Daemon', daemon_options) do
            loop do
                plugins.each { |p|
                    plugin = Object.const_get(p).new
                    plugin.execute
                }
                sleep(config['Interval'])
            end
        end
    end
end

monitor = Monitor.new