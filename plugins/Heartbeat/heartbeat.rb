require 'open-uri'

class Heartbeat 
	
	def initialize
		if (File.exists? 'heartbeat.id')
			@id = File.open('heartbeat.id', 'r').read
		end

	end

	def execute
		URI.parse('http://shrouded-badlands-9066.herokuapp.com/devices/heartbeat/1').read
	end


end