# System Plugin
#
# A base plugin included within the Monitor daemon package to provide logging of:
#
# - Process ID
# - Hostname
# - Timestamp
# - IP Address
# - Mac Address
# - CPU Load Average [1 minute, 5 minutes, 15 minutes]
#
# Author:: Phil Kershaw <mailto:p.a.kershaw@gmail.com>
# Package:: Monitor/Plugins/System

require 'socket'
require 'sys/cpu'
require 'macaddr'
include Sys

class System
    attr_reader :timestamp, :process_id, :hostname, :load_average, :ip, :mac_address
    
    def initialize
        @timestamp    = Time.now
        @process_id   = Process.pid
        @hostname     = Socket.gethostname
        @load_average = CPU.load_avg
        @ip           = Socket.ip_address_list.detect{|intf| intf.ipv4_private?}.ip_address()
        @mac_address  = Mac.address
    end
    
    def execute
        path = $log_path
        log_file = File.open(File.join(path, "system-#{@timestamp.to_i}.log"), 'w')
        log_detail = <<-OUTPUT
[#{@timestamp}] 
PID: 
    #{@process_id}
Load Average [1 min, 5 min, 15 min]: 
    #{@load_average}
Hostname:
    #{@hostname}
IP Address: 
    #{@ip}
MAC Address: 
    #{@mac_address}
OUTPUT

        log_file.puts log_detail
        log_file.close
    end 
end