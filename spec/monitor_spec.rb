require './spec_helper'

describe Monitor do
    
    before :each do
        @Monitor = Monitor.new   
    end
    
    describe "#new" do
        it "should return a new instance of book" do
            @Monitor.should be_instance_of Monitor  
        end
        
        it "should set the global log variable" do
            $log_path.should_not be_nil
        end
    end
end

describe Heartbeat do

    before :each do
        @Heartbeat = Heartbeat.new
    end

    describe "#new" do
        it "should have an id set" do
            @Heartbeat.id.should_not be_nil
        end

        
    end    
end