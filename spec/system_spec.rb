require './spec_helper'

describe System do
    before :each do
        @System = System.new
    end
    
    after :each do
        # FileUtils.rm_rf("system-#{@System.timestamp}.log")
    end
    
    describe "#new" do
        it "should return a new instance of the system object" do
            @System.should be_an_instance_of System
        end
        
        it "should set the timestamp" do
            @System.timestamp.should be_within(1).of(Time.now)
        end
        
        it "should set the process id" do
            @System.process_id.should be_an_instance_of Fixnum
        end
        
        it "should set the hostname" do
            require 'socket'
            @System.hostname.should be_an_instance_of String
        end
    
        it "should set the current load average" do
            require "sys/cpu"
            include Sys
            @System.load_average.should be_an_instance_of Array
        end
    
        it "should set the ip address" do
            @System.ip.should =~ /\b(?:\d{1,3}\.){3}\d{1,3}\b/
        end
    
        it "should set the mac address" do
            @System.mac_address.should =~ /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/
        end
    end
    
    describe "#execute" do
        it "should create a system log file" do
            $log_path = './'
            @System.execute
            Dir.glob("system-#{@System.timestamp.to_i}.log").length.should eql 1
        end
    end
end